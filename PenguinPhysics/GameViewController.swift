//
//  GameViewController.swift
//  PenguinPhysics
//
//  Created by Jake Gundersen on 7/23/14.
//  Copyright (c) 2014 Razeware, LLC. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController, SCNSceneRendererDelegate {
    
    @IBOutlet var scnView: SCNView!
    let scene = SCNScene(named: "SceneIceRotated.dae")!
    let cube: SCNNode
    var applyForce = false
    var force: Float = 50.0
    let slopeNode: SCNNode
    var sliderJoint: SCNPhysicsSliderJoint!
    let snow = SCNParticleSystem(named: "Snow", inDirectory: nil)
    
    required init(coder aDecoder: NSCoder) {
        cube = scene.rootNode.childNodeWithName("Cube", recursively: false)!
        slopeNode = SCNNode()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scnView.scene = scene
        scnView.allowsCameraControl = true
        scnView.showsStatistics = true
        scnView.delegate = self
        
        scene.physicsWorld.gravity = SCNVector3(x: 0.0, y: -9.8, z: 0.0)
        cube.physicsBody = SCNPhysicsBody.dynamicBody()
        cube.physicsBody!.mass = 5.0
        cube.physicsBody!.restitution = 0.01
        
        if let slope = scene.rootNode.childNodeWithName("Slope", recursively: false) {
            slope.physicsBody = SCNPhysicsBody.kinematicBody()
            
            slopeNode.pivot = SCNMatrix4MakeTranslation(-7.0, 0.0, 0.0)
            slopeNode.position = SCNVector3Make(-7.0, 0.0, 0.0)
            
            slopeNode.addChildNode(slope)
            scene.rootNode.addChildNode(slopeNode)
            
            sliderJoint = SCNPhysicsSliderJoint(
                bodyA: cube.physicsBody,
                axisA: SCNVector3(x: 0.0, y: -1.0, z: 0.0),
                anchorA: SCNVector3(x: 0.0, y: 0.0, z: -1.0),
                bodyB: slope.physicsBody,
                axisB: SCNVector3(x: 0.0, y: -1.0, z: 0.0),
                anchorB: SCNVector3(x: 0.0, y: 0.0, z: -0.20))
            
            sliderJoint.maximumLinearLimit = 4.0
            sliderJoint.minimumLinearLimit = -5.5
            
            sliderJoint.maximumAngularLimit = 0.0
            sliderJoint.minimumAngularLimit = 0.0
            
            scene.physicsWorld.addBehavior(sliderJoint)
            
            cube.physicsBody!.categoryBitMask = 1
            cube.physicsBody!.collisionBitMask = 1
            slope.physicsBody!.categoryBitMask = 2
            slope.physicsBody!.collisionBitMask = 2
        }
        
        let particleEmitterNode = SCNNode()
        particleEmitterNode.addParticleSystem(snow)
        particleEmitterNode.position = SCNVector3(x: 0.0, y: 10.0, z: 0.0)
        
        scene.rootNode.addChildNode(particleEmitterNode)
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Landscape.rawValue)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    @IBAction func forceSliderMoved(sender: UISlider) {
        force = sender.value * 300.0
    }
    
    @IBAction func angleSliderMoved(sender: UISlider) {
        var angle: Float = (1.0 - sender.value) * Float(M_PI_4) //Converts angle to radians
        slopeNode.eulerAngles = SCNVector3(x: 0.0, y: 0.0, z: angle)
    }
    
    @IBAction func applyForceTapped(sender: UIButton) {
        if applyForce {
            sender.setTitle("Apply Force", forState: .Normal)
            applyForce = false
        } else {
            sender.setTitle("Stop", forState: .Normal)
            applyForce = true
        }
    }
    
    //MARK: SCNSceneRendererDelegate callbacks -
    func renderer(aRenderer: SCNSceneRenderer, updateAtTime time: NSTimeInterval) {
        if applyForce {
            cube.physicsBody!.applyForce(SCNVector3(x: force, y: 0.0, z: 0.0), impulse: false)
        }
    }

}
